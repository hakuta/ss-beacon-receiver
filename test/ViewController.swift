import UIKit
import CoreLocation
import UserNotifications

class ViewController: UIViewController, CLLocationManagerDelegate, UNUserNotificationCenterDelegate {
    var myLocationManager: CLLocationManager!
    var myBeaconRegion: CLBeaconRegion!
    var beaconUuids: NSMutableArray!
    var beaconDetails: NSMutableArray!

    let UUIDList = [
        "48534442-4C45-4144-80C0-1800FFFFFFF1",
        "719F1869-2F7D-4BD2-9F57-12BE902C7166"
    ]
    // push通知のデータ。本来はサーバから取得するが時間削減のためローカルに持つ。
    let advertisingData = [
        101: [
            0: [
                "title": "日焼けサロンいしかわ",
                "subtitle": "",
                "body": "期間限定の30%OFFキャンペーン実施中",
                "imagePath": "image_101"
            ]
        ],
        201: [
            0: [
                "title": "コーヒーショップいしかわ",
                "subtitle": "",
                "body": "本日のコーヒーはグアテマラ産です",
                "imagePath": "image_201"
            ]
        ],
        301: [
            0: [
                "title": "ベーカリーいしかわ",
                "subtitle": "",
                "body": "フレンチトースト焼き上がりました",
                "imagePath": "image_301"
            ]
        ]
    ]
    var pushReceivedFlag = [
        101: [ 0: false ],
        201: [ 0: false ],
        301: [ 0: false ]
    ]
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var adImage: UIImageView!
    @IBOutlet weak var majorIDLabel: UILabel!
    @IBOutlet weak var minorIDLabel: UILabel!
    @IBOutlet weak var couponGetButton: UIButton!
    @IBAction func couponGetButton(_ sender: UIButton) {
        let alert = UIAlertController(title: "クーポン獲得", message: "お得なクーポンを獲得しました", preferredStyle: .alert)

        alert.addAction(
            UIAlertAction(
                title: "OK",
                style: .default
            )
        )
        present(alert, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        myLocationManager = CLLocationManager()
        myLocationManager.delegate = self
        myLocationManager.desiredAccuracy = kCLLocationAccuracyBest
        myLocationManager.distanceFilter = 1
        let status = CLLocationManager.authorizationStatus()
        print("CLAuthorizedStatus: \(status.rawValue)")
        if(status == .notDetermined) {
            myLocationManager.requestAlwaysAuthorization()
        }
        beaconUuids = NSMutableArray()
        beaconDetails = NSMutableArray()

        //ローカル通知初期設定
        notificationSetting()
        couponGetButton.isHidden = true
    }

    private func startMyMonitoring() {
        print ("startMyMonitoring")
        print (UUIDList.count)

        for i in 0 ..< UUIDList.count {
            let uuid: NSUUID! = NSUUID(uuidString: "\(UUIDList[i].lowercased())")
            let identifierStr: String = "abcde\(i)"
            myBeaconRegion = CLBeaconRegion(proximityUUID: uuid as UUID, identifier: identifierStr)
            myBeaconRegion.notifyEntryStateOnDisplay = false
            myBeaconRegion.notifyOnEntry = true
            myBeaconRegion.notifyOnExit = true
            myLocationManager.startMonitoring(for: myBeaconRegion)
        }
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("didChangeAuthorizationStatus")
        switch (status) {
        case .notDetermined:
            print("not determined")
            break
        case .restricted:
            print("restricted")
            break
        case .denied:
            print("denied")
            break
        case .authorizedAlways:
            print("authorizedAlways")
            startMyMonitoring()
            break
        case .authorizedWhenInUse:
            print("authorizedWhenInUse")
            startMyMonitoring()
            break
        }
    }

    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        manager.requestState(for: region)
    }

    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        print("!!!!")
        switch (state) {
        case .inside:
            print("iBeacon inside")
            manager.startRangingBeacons(in: region as! CLBeaconRegion)
            break
        case .outside:
            print("iBeacon outside")
            break
        case .unknown:
            print("iBeacon unknown")
            break
        }
    }

    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        beaconUuids = NSMutableArray()
        beaconDetails = NSMutableArray()

        if(beacons.count > 0) {
            for i in 0 ..< beacons.count {
                let beacon = beacons[i]
                let beaconUUID = beacon.proximityUUID
                let minorID = beacon.minor
                let majorID = beacon.major
                var proximity = ""
                var title = ""
                var imagePath = ""
                if(advertisingData[majorID.intValue]?[minorID.intValue] != nil) {
                    title = (advertisingData[majorID.intValue]?[minorID.intValue]?["title"])!
                    imagePath = (advertisingData[majorID.intValue]?[minorID.intValue]?["imagePath"])!
                    let adImageSource = UIImage(named: imagePath + ".jpeg")!
                    adImage.image = adImageSource
                }
                couponGetButton.isHidden = true

                switch (beacon.proximity) {
                case CLProximity.unknown :
                    print("Proximity: Unknown")
                    proximity = "距離: 不明"
                    break
                case CLProximity.far:
                    print("Proximity: Far")
                    proximity = "距離: 遠い"
                    break
                case CLProximity.near:
                    print("Proximity: Near")
                    proximity = "距離: 近い"
                    break
                case CLProximity.immediate:
                    print("Proximity: Immediate")
                    proximity = "距離: すごく近い"
                    couponGetButton.isHidden = false

                    // 広告データがある場合は通知
                    if(advertisingData[majorID.intValue]?[minorID.intValue] != nil) {
                        // 一旦、一度送信した通知は再起動するまで再送信はしない
                        if (pushReceivedFlag[majorID.intValue]?[minorID.intValue] == false) {
                            pushReceivedFlag[majorID.intValue]?[minorID.intValue] = true
                            let title     = advertisingData[majorID.intValue]?[minorID.intValue]?["title"]
                            let subtitle  = advertisingData[majorID.intValue]?[minorID.intValue]?["subtitle"]
                            let body      = advertisingData[majorID.intValue]?[minorID.intValue]?["body"]
                            let imagePath = advertisingData[majorID.intValue]?[minorID.intValue]?["imagePath"]
                            setNotification(title: title ?? "", subtitle: subtitle ?? "", body: body ?? "", imagePath: imagePath ?? "")
                        }
                    }
                    break
                }
                beaconUuids.add(beaconUUID.uuidString)
                var myBeaconDetails = "Major: \(majorID) "
                myBeaconDetails += "Minor: \(minorID) "
                myBeaconDetails += "Proximity:\(proximity) "
                print(myBeaconDetails)
                beaconDetails.add(myBeaconDetails)
                label1.text = proximity
                majorIDLabel.text = "広告主: " + title
                minorIDLabel.text = ""
            }
        }
    }

    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        print("didEnterRegion: iBeacon found")
        manager.startRangingBeacons(in: region as! CLBeaconRegion)
    }

    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        print("didExitRegion: iBeacon lost")
        manager.stopRangingBeacons(in: region as! CLBeaconRegion)
    }

    func locationManagerDidPauseLocationUpdates(manager: CLLocationManager) {
        print("位置情報更新停止")
    }

    func locationManagerDidResumeLocationUpdates(manager: CLLocationManager) {
        print("位置情報更新再開")
    }

    // 通知を作成
    func setNotification(title: String, subtitle: String, body: String, imagePath: String) {
        print ("setNotification")
        // タイトル、本文、サウンド設定の保持
        let content = UNMutableNotificationContent()
        content.title = title
        content.subtitle = subtitle
        content.body = body
        content.sound = UNNotificationSound.default
        //content.sound = UNNotificationSound(named:UNNotificationSoundName(rawValue: "btn01.mp3"))

        if let path = Bundle.main.path(forResource: imagePath, ofType: "jpeg") {
            content.attachments = [try! UNNotificationAttachment(identifier: "ID1", url: URL(fileURLWithPath: path), options: nil)]
        }

        // seconds後に起動するトリガーを保持
        //let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.1, repeats: false)

        let generalCategory = UNNotificationCategory(identifier: "GENERAL",
                                                     actions: [],
                                                     intentIdentifiers: [],
                                                     options: .customDismissAction)
        // TIMER_EXPIREDカテゴリのカスタムアクションを作成。
        let snoozeAction = UNNotificationAction(identifier: "SNOOZE_ACTION",
                                                title: "Snooze",
                                                options: .foreground)
                                                //options: UNNotificationActionOptions(rawValue: 0))
        let stopAction = UNNotificationAction(identifier: "STOP_ACTION",
                                              title: "Stop",
                                              options: .foreground)

        let expiredCategory = UNNotificationCategory(identifier: "TIMER_EXPIRED",
                                                     actions: [snoozeAction, stopAction],
                                                     intentIdentifiers: [],
                                                     options: .customDismissAction)
        //content.categoryIdentifier = expiredCategory.identifier

        // 識別子とともに通知の表示内容とトリガーをrequestに内包する
        // triggerをnilにすると即時通知
        let request = UNNotificationRequest(identifier: "campaign", content: content, trigger: nil)

        // UNUserNotificationCenterにrequestを加える
        let center = UNUserNotificationCenter.current()
        //center.setNotificationCategories([generalCategory, expiredCategory])
        center.delegate = self
        center.add(request) { (error) in
            if let error = error {
                print(error.localizedDescription)
            }
        }
    }

    func notificationSetting() {
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
            // 許可の有無に応じて機能を有効かまたは無効化。
            if error != nil {
                return
            }

            if granted {
                print("通知許可")
                let center = UNUserNotificationCenter.current()
                center.delegate = self

            } else {
                print("通知拒否")
            }
        }
    }
    // フォアグラウンドの場合でも通知を表示する
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
}
