//
//  TutorialViewController.swift
//  test
//
//  Created by Akutagawa Hiroyuki on 2018/09/27.
//  Copyright © 2018 Akutagawa Hiroyuki. All rights reserved.
//
import UIKit

class TutorialViewController: UIViewController, UIScrollViewDelegate {
    var scrollView: UIScrollView!
    var pageControll: UIPageControl!
    let pageNum = 4
    let pageColors:[Int:UIColor] = [1:UIColor.red,2:UIColor.yellow,3:UIColor.blue,4:UIColor.green]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.scrollView = UIScrollView(frame: self.view.bounds)
        self.scrollView.contentSize = CGSize(width: self.view.bounds.width * CGFloat(pageNum), height: self.view.bounds.height)
        self.scrollView.isPagingEnabled = true
        self.scrollView.showsHorizontalScrollIndicator = false
        self.scrollView.delegate = self;
        self.view.addSubview(self.scrollView)
        
        self.pageControll = UIPageControl(frame: CGRect(x: 0, y: self.view.bounds.height-50, width: self.view.bounds.width, height: 50))
        self.pageControll.numberOfPages = pageNum
        self.pageControll.currentPage = 0
        self.view.addSubview(self.pageControll)
        
        for p in 1...pageNum {
            let v = UIView(frame: CGRect(x: self.view.bounds.width * CGFloat(p-1), y: 0, width: self.view.bounds.width, height: self.view.bounds.height))
            v.backgroundColor = self.pageColors[p]
            if (p == 1) {
                let label = UILabel()
                label.text = "現SS商品への危機感（！）から、別基軸の商品を模索できないか検討しました。"
                label.textColor = UIColor.white
                label.frame =  CGRect(x: 50,y: 200, width: 300,height: 200)
                label.numberOfLines = 10
                v.addSubview(label)
            }
            if (p == 2) {
                let label = UILabel()
                label.text = "まだ現SS商品媒体価値を感じられていますが、やはりデジタル広告へシフトしていくはずです。"
                label.textColor = UIColor.black
                label.frame =  CGRect(x: 50,y: 200, width: 300,height: 200)
                label.numberOfLines = 10
                v.addSubview(label)
            }
            if (p == 3) {
                let label = UILabel()
                label.text = "既存顧客とのコミュニケーション、\nおよびその集客はSNSへ集約されそうです。\n\n新規顧客の認知獲得および集客は、新聞折込・ポスティングから何か別の媒体へシフトが起こるでしょう。"
                label.textColor = UIColor.white
                label.frame =  CGRect(x: 50,y: 200, width: 300,height: 200)
                label.numberOfLines = 30
                label.lineBreakMode = .byWordWrapping
                v.addSubview(label)
            }
            if (p == 4) {
                let label = UILabel()
                label.text = "beaconを使った集客アプリ（デモ）を作りました。"
                label.textColor = UIColor.black
                label.frame =  CGRect(x: 50,y: 200, width: 300,height: 200)
                label.numberOfLines = 10
                v.addSubview(label)
                
                //ボタンを押したときの動作
                let btn = UIButton()
                btn.frame = CGRect(x: 50,y: CGFloat(5)*30, width: 300,height: 50)

                //btn.addTarget(self, action: Selector(("pushed:")), for: .touchUpInside)
                //btn.addTarget(self, action: Selector("tapped:"), for:.touchUpInside)
                btn.addTarget(self, action: #selector(self.tapped(_:)), for:.touchUpInside)

                //見える用に赤くした
                btn.backgroundColor = UIColor.red
                btn.layer.position = CGPoint(x: self.view.frame.width/2, y:self.view.frame.height*4/5)
                btn.setTitle("はじめてみよう", for: .normal)

                v.addSubview(btn)
            }
            self.scrollView.addSubview(v)
        }
    }

    @objc func tapped(_ button : UIButton) {
        print("tapped")
        
        let next = self.storyboard?.instantiateViewController(withIdentifier: "1") as! ViewController
        print(next)
        // Showならこちら
        self.present(next, animated: true)
        //self.performSegue(withIdentifier: "1", sender: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageProgress = Double(scrollView.contentOffset.x / scrollView.bounds.width)
        self.pageControll.currentPage = Int(round(pageProgress))
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
